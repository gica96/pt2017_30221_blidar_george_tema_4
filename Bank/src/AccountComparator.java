import java.io.Serializable;
import java.util.Comparator;

public class AccountComparator implements Comparator<Account>,Serializable {
	
	private static final long serialVersionUID = 1L;

	public int compare(Account a1, Account a2)
	{
		if(a1.equals(a2))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}
