
public interface BankProc {
	
	public void addPerson(Person person);
	public void removePerson(Person person);
	public void addAccount(Account account);
	public void removeAccount(Account account);
	public void readPersons();
	public void writePersons();
	public void readAccounts();
	public void writeAccounts();
	//public void getPersonUpdates(Person person);
	
}