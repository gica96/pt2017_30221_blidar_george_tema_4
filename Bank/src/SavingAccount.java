import javax.swing.JOptionPane;

public class SavingAccount extends Account {

	private static final long serialVersionUID = 1L;
	
	private int numberOfWithdrawals;
	private double balance;
	private double interestRate;
	
	public SavingAccount(int id, Person owner, int money)
	{
		super(id,owner,money,"Saving Account");
		numberOfWithdrawals = 0;
		balance = money / 2;
		interestRate = 0.25 * money; 
	}
	
	public int getNumberOfWithdrawals()
	{
		return this.numberOfWithdrawals;
	}
	
	public void setNumberOfWithdrawals(int value)
	{
		this.numberOfWithdrawals = value;
	}
	
	public double getBalance()
	{
		return this.balance;
	}
	
	public double getInterestRate()
	{
		return this.interestRate;
	}
	
	public boolean validateAccount()
	{
		if(this.startingMoney < 500)
		{
			return false;
		}
		else if(this.numberOfWithdrawals > 3)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public void addMoney(int money)
	{
		try
		{
			if(money > 0)
			{
				this.currentMoney = this.currentMoney + money;
				this.balance =  this.currentMoney / 2;
				this.interestRate = 0.25 * this.currentMoney;
				notifyObserver();
			}
			else
			{
				throw new AssertionError("Introduceti o suma corecta!");
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
	}
	
	public void withdrawMoney(int money)
	{
		try
		{
			if(money <= 0)
			{
				throw new AssertionError("Suma invalida!");
			}
			else if(this.numberOfWithdrawals > 3)
			{
				throw new AssertionError("Ati depasit numarul de extrageri pentru acest cont!");
			}
			else if(money > balance)
			{
				throw new AssertionError("Nu puteti extrage o suma mai mare decat valoarea balance-ului!");
			}
			else if(money > this.currentMoney)
			{
				throw new AssertionError("Nu aveti atatia bani pe card!");
			}
			else
			{
				this.currentMoney = this.currentMoney - money;
				this.balance = this.currentMoney / 2;
				this.interestRate = 0.25 * this.currentMoney;
				this.numberOfWithdrawals ++;
				notifyObserver();
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
	}
	
	public void notifyObserver()
	{
		this.holder.update(this, this);
	}
}