import javax.swing.JOptionPane;

public class SpendingAccount extends Account {

	private static final long serialVersionUID = 1L;
	
	public SpendingAccount(int id, Person holder, int money)
	{
		super(id,holder,money,"Spending Account");
	}
	
	public boolean validateAccount()
	{
		if(this.startingMoney == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public void addMoney(int money)
	{
		try
		{
			if(money > 0)
			{
				this.currentMoney = this.currentMoney + money;
				notifyObserver();
			}
			else
			{
				throw new AssertionError("Introduceti o suma corecta!");
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
	}
	
	public void withdrawMoney(int money)
	{
		try
		{
			if(money <= 0)
			{
				throw new AssertionError("Introduceti o suma corecta!");
			}
			else if(money > this.currentMoney)
			{
				throw new AssertionError("Nu aveti atatia bani pe card!");
			}
			else
			{
				this.currentMoney = this.currentMoney - money;
				notifyObserver();
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
	}
	
	public void notifyObserver()
	{
		this.holder.update(this, this);
	}
}
