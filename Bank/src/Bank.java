import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import javax.swing.JOptionPane;

public class Bank implements BankProc{
	
	private HashMap<Person,TreeSet<Account>> map;
	private ArrayList<Person> persons;
	
	public Bank()
	{
		map = new HashMap<Person,TreeSet<Account>>();
		persons = new ArrayList<Person>();
	}
	
	public HashMap<Person,TreeSet<Account>> getMap()
	{
		return this.map;
	}
	
	public ArrayList<Person> getPersons()
	{
		return this.persons;
	}
	
	/**
	 * @precondition p.validatePerson() == true
	 * @postcondition persons.size() = map.size() + 1;
	 */
	public void addPerson(Person p)
	{
		try
		{
			assert p.validatePerson() : "Persoana introdusa nu este valida!";
			int size = persons.size();
			boolean ok = true;
			for(Person pers : this.persons)
			{
				if(p.getName().equals(pers.getName()))
				{
					ok = false;
					break;
				}
			}
			if(ok)
			{
				persons.add(p);
				assert (persons.size() == (size + 1)) : "Problema cu size-ul ArrayList-ului de Person";
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Nu se pot adauga doua persoane identice!");
			}
			
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, "Invalid person!");
		}
	}
	
	/**
	 * @precondition persons.size() > 0
	 * @precondition p != null
	 * @precondition persons.contains(p) == true
	 * @postcondition persons.size() = persons.size() - 1
	 */
	public void removePerson(Person p)
	{
		try
		{
			assert persons.size() > 0 : "ArrayList-ul este gol!";
			assert p != null : "Persoana este null";
			assert persons.contains(p) : "Aceasta persoana nu exista creata!";
			int size = this.persons.size();
			
			this.persons.remove(p);
			assert (persons.size() == (size - 1)) : "Problema cu size-ul ArrayList-ului de Person";
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, "This person cannot be deleted! Check if it exists!");
		}
	}
	
	/**
	 * Depending on the type of the account, we check it's validity. Also, we check if the holder is registered
	 * @precondition account.validateAccount() == true
	 * @precondition persons.contains(account.getHolder) == true
	 * @postcondition (map.get(account.getHolder()).size() == (size + 1))
	 */
	public void addAccount(Account account)
	{
		try
		{
			String s = account.getType();
			if(s.equals("Saving Account"))
			{
				assert(((SavingAccount)account).validateAccount()) : "Account-ul de Saving nu este valid!";
				assert(persons.contains(account.getHolder())) : "Persoana nu a fost creata inainte!";
				TreeSet<Account> set;
				int size = -1;
				
				if(map.get(account.getHolder()) != null)
				{
					 set = map.get(account.getHolder());
					 size = set.size();
					 set.add(account);
					 map.put(account.getHolder(), set);
				}
				else
				{
					set = new TreeSet<Account>(new AccountComparator());
					size = set.size();
					set.add(account);
					map.put(account.getHolder(), set);
				}
				 if(size == set.size())
				 {
					 JOptionPane.showMessageDialog(null, "Avertisment! S-a incercat introducerea unui Saving Account deja existent pe numele: " + account.getHolder().getName());
				 }
				 else
				 {
					assert (map.get(account.getHolder()).size() == (size + 1)) : "Problema cu size-ul dupa introducerea unui nou Saving Account";
				 }
			}
			else if(s.equals("Spending Account"))
			{
				assert(((SpendingAccount)account).validateAccount()) : "Account-ul de Spending nu este valid!";
				assert(persons.contains(account.getHolder())) : "Persoana nu a fost creata inainte!";
				TreeSet<Account> set;
				int size = -1;
				
				if(map.get(account.getHolder()) != null)
				{
					 set = map.get(account.getHolder());
					 size = set.size();
					 set.add(account);
					 map.put(account.getHolder(), set);
				}
				else
				{
					set = new TreeSet<Account>(new AccountComparator());
					size = set.size();
					set.add(account);
					map.put(account.getHolder(), set);
				}
				 if(size == set.size())
				 {
					 JOptionPane.showMessageDialog(null, "Avertisment! S-a incercat introducerea unui Spending Account deja existent pe numele: " + account.getHolder().getName());
				 }
				 else
				 {
					assert (map.get(account.getHolder()).size() == (size + 1)) : "Problema cu size-ul dupa introducerea unui nou Spending Account";
				 }
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
	}
	
	/**
	 * @precondition map.size() > 0
	 * @precondition account != null
	 * @precondition map.get(account.getHolder()).contains(account) == true
	 * @postcondition (map.get(account.getHolder()).size() == (size - 1))
	 * 
	 */
	public void removeAccount(Account account)
	{
		try
		{
			assert map.size() > 0 : "HashMap-ul este gol!";
			assert account != null : "Account null!";
			assert map.get(account.getHolder()).contains(account) == true : "Acest account nu este detinut de catre persoana: " + " " + account.getHolder().getName();
			
			int size = map.get(account.getHolder()).size();
			
			map.get(account.getHolder()).remove(account);
			
			assert((map.get(account.getHolder()).size() == (size-1))) : "Problema cu size-ul dupa stergerea unui account!";
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
	}
	
	/**
	 * @precondition persons.size() == 0;
	 */
	
	@SuppressWarnings("unchecked")
	public void readPersons()
	{
		try
		{
			assert persons.size() == 0 : "La inceputul rularii programului, lista de persoane nu este goala!";
			InputStream personFile = new FileInputStream("D:\\Proiecte Java\\Bank\\persons.ser");
			ObjectInputStream objPerson = new ObjectInputStream(personFile);
			try
			{
				persons = (ArrayList<Person>) objPerson.readObject();
			}
			catch(ClassNotFoundException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				personFile.close();
				objPerson.close();
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Warning! First time reading from persons.ser!!");
		}
	}
	
	/**
	 * @postcondition persons.size >=0
	 */
	
	public void writePersons()
	{
		try
		{
			assert persons.size() >= 0 : "Person list size is 0 when writing persons!";
			OutputStream personFile = new FileOutputStream("D:\\Proiecte Java\\Bank\\persons.ser");
			ObjectOutput objPerson = new ObjectOutputStream(personFile);
			try
			{
				objPerson.writeObject(persons);
			}
			finally
			{
				personFile.close();
				objPerson.close();
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Eroare la scrierea in fisierul stream!");
		}
	}
	
	/**
	 * @precondition map.size() == 0
	 */
	@SuppressWarnings("unchecked")
	public void readAccounts()
	{
		try
		{
			assert map.size() == 0 : "La inceputul rularii programului, lista de persoane nu este goala!";
			InputStream accountFile = new FileInputStream("D:\\Proiecte Java\\Bank\\accounts.ser");
			ObjectInputStream objAccount = new ObjectInputStream(accountFile);
			try
			{
				map = (HashMap<Person,TreeSet<Account>>) objAccount.readObject();
			}
			catch(ClassNotFoundException ex)
			{
				ex.printStackTrace();
			}
			finally
			{
				accountFile.close();
				objAccount.close();
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Warning! First time reading from accounts.ser!!");
		}
	}
	
	/**
	 * @precondition map.size > 0
	 */
	public void writeAccounts()
	{
		try
		{
			assert map.size() >= 0 : "Map size is 0 when writing persons!";
			OutputStream accountFile = new FileOutputStream("D:\\Proiecte Java\\Bank\\accounts.ser");
			ObjectOutput objAccount = new ObjectOutputStream(accountFile);
			try
			{
				objAccount.writeObject(map);
			}
			finally
			{
				accountFile.close();
				objAccount.close();
			}
		}
		catch(AssertionError ex)
		{
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Eroare la scrierea in fisierul stream!");
		}
	}
}
