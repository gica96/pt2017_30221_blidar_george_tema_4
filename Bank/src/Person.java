import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Pattern;

public class Person implements Serializable, Observer {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String adress;
	private String personId;
	private ArrayList<String> status;
	
	public Person(String name, String adress, String id)
	{
		this.name = name;
		this.adress = adress;
		this.personId = id;
		status = new ArrayList<String>();
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getAdress()
	{
		return this.adress;
	}
	
	public String getId()
	{
		return this.personId;
	}
	
	public ArrayList<String> getStatus()
	{
		return this.status;
	}
	
	public boolean validatePerson()
	{
		if(this.name.equals(""))
		{
			return false;
		}
		else if(this.adress.equals(""))
		{
			return false;
		}
		else if(this.personId.equals(""))
		{
			return false;
		}
		else if(this.personId.length() != 10)
		{
			return false;
		}
		else if(Pattern.matches("^[0-9]*$", this.personId) == false && this.personId.length() == 10)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	@Override
	public int hashCode()
	{
		return this.name.hashCode() + this.personId.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(!(o instanceof Person))
		{
			return false;
		}
		Person p = (Person) o;
		return this.name.equals(p.name) && this.personId.equals(p.personId);
	}
	
	@Override
	public void update(Observable o, Object arg) 
	{
		if(arg instanceof SavingAccount)
		{
			SavingAccount account = (SavingAccount) o;
			String s = "Contul dumneavoastra cu id-ul: " + account.getId() + " de tipul: " + account.getType() + " tocmai a fost updatat, domnule " + account.getHolder().getName() + "!" + " Pe acest cont mai aveti: " + account.getSum() + " RON, cu un balance de: " + account.getBalance() + " si dobanda de: " + account.getInterestRate();
			status.add(s);
		}
		else if(arg instanceof SpendingAccount)
		{
			SpendingAccount account = (SpendingAccount) o;
			String s = new String("Contul dumneavoastra cu id-ul: " + account.getId() + " de tipul: " + account.getType() + " tocmai a fost updatat, domnule " + account.getHolder().getName() + "!" + " Pe acest cont mai aveti: " + account.getSum() + " RON!");
			status.add(s);
		}		
	}
}
