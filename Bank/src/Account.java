import java.io.Serializable;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	protected int accountId;
	protected Person holder;
	protected int currentMoney;
	protected int startingMoney;
	protected String accountType;
	
	public Account(int id, Person owner, int money, String type)
	{
		this.accountId = id;
		this.holder = owner;
		this.currentMoney = money;
		this.startingMoney = money;
		this.accountType = type;
	}
	
	public int getId()
	{
		return this.accountId;
	}
	
	public Person getHolder()
	{
		return this.holder;
	}
	
	public int getSum()
	{
		return this.currentMoney;
	}
	
	public String getType()
	{
		return this.accountType;
	}
	
	@Override 
	public int hashCode()
	{
		return this.accountId + this.accountType.hashCode();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(!(o instanceof Account))
		{
			return false;
		}
		Account a = (Account) o;
		return (this.accountId == a.accountId && this.accountType.equals(a.accountType));
	}
	public abstract void notifyObserver();
}
