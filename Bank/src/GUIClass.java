import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class GUIClass extends JPanel implements ActionListener {

	static final long serialVersionUID = 1L;
	
	public static void triggerWindow()
	{
		JFrame window = new JFrame("Bank");
		GUIClass content = new GUIClass();
		window.setContentPane(content);
		window.pack();
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
	}
		
	JLabel personName;
	JTextField personNameField;
	JLabel personAdress;
	JTextField personAdressField;
	JLabel personId;
	JTextField personIdField;
	JButton addPerson;
	
	JLabel personNameDelete;
	JTextField personNameDeleteField;
	JButton deletePerson;
	
	JLabel accountHolderName;
	JTextField accountHolderNameField;
	JLabel startingSum;
	JTextField startingSumField;
	JComboBox<String> accountTypes;
	JButton addAccount;
	
	JLabel holderName;
	JTextField holderNameField;
	JLabel accountId;
	JTextField accountIdField;
	JButton deleteAccount;
	
	JLabel withdrawal;
	JTextField withdrawalField;
	JButton withdrawalButton;
	
	JLabel deposit;
	JTextField depositField;
	JButton depositButton;
	
	DefaultTableModel modelPerson;
	JTable tablePerson;
	DefaultTableModel modelAccount;
	JTable tableAccount;
	
	JScrollPane scrollPerson;
	JScrollPane scrollAccount;
	int row;
	int col;
	
	private FileWriter writer;
	
	Bank bank;
	JButton closeFile;
	
	
	public GUIClass()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.GREEN);
		this.row = -1;
		this.col = -1;
		
		
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(3,2));
		centerPanel.setBackground(Color.GREEN);
		this.add(centerPanel);
		
		JPanel centerPanel1 = new JPanel();
		centerPanel1.setLayout(new BorderLayout());
		centerPanel1.setBackground(Color.GREEN);
		centerPanel.add(centerPanel1);
		
		JPanel centerPanel1BL = new JPanel();
		centerPanel1BL.setLayout(new GridLayout(3,2));
		centerPanel1BL.setBackground(Color.GREEN);
		centerPanel1.add(centerPanel1BL,BorderLayout.CENTER);
	
		personName = new JLabel("Person's Name:");
		centerPanel1BL.add(personName);
		personNameField = new JTextField();
		centerPanel1BL.add(personNameField);
		personAdress = new JLabel("Person's Adress:");
		centerPanel1BL.add(personAdress);
		personAdressField = new JTextField();
		centerPanel1BL.add(personAdressField);
		personId = new JLabel("Person's Id:");
		centerPanel1BL.add(personId);
		personIdField = new JTextField();
		centerPanel1BL.add(personIdField);
		addPerson = new JButton("Add Person");
		addPerson.setBounds(50,50,10,10);
		addPerson.addActionListener(this);
		centerPanel1.add(addPerson,BorderLayout.SOUTH);
		
		
		JPanel centerPanel2 = new JPanel();
		centerPanel2.setLayout(new BorderLayout());
		centerPanel2.setBackground(Color.GREEN);
		centerPanel.add(centerPanel2);
		
		JPanel centerPanel2BL = new JPanel();
		centerPanel2BL.setLayout(new GridLayout(1,1));
		centerPanel2BL.setBackground(Color.GREEN);
		centerPanel2.add(centerPanel2BL,BorderLayout.CENTER);
		
		personNameDelete = new JLabel("Person's Name:");
		centerPanel2BL.add(personNameDelete);
		personNameDeleteField = new JTextField();
		centerPanel2BL.add(personNameDeleteField);
		deletePerson = new JButton("Delete Person");
		deletePerson.addActionListener(this);
		centerPanel2.add(deletePerson,BorderLayout.SOUTH);
		
		JPanel centerPanel3 = new JPanel();
		centerPanel3.setLayout(new BorderLayout());
		centerPanel3.setBackground(Color.GREEN);
		centerPanel.add(centerPanel3);
		
		JPanel centerPanel3BL = new JPanel();
		centerPanel3BL.setLayout(new GridLayout(3,2));
		centerPanel3BL.setBackground(Color.GREEN);
		centerPanel3.add(centerPanel3BL,BorderLayout.CENTER);
		
		accountHolderName = new JLabel("Holder's Name:");
		centerPanel3BL.add(accountHolderName);
		accountHolderNameField = new JTextField();
		centerPanel3BL.add(accountHolderNameField);
		startingSum = new JLabel("Starting Sum:");
		centerPanel3BL.add(startingSum);
		startingSumField = new JTextField();
		centerPanel3BL.add(startingSumField);
		accountTypes = new JComboBox<String>();
		centerPanel3BL.add(accountTypes);
		accountTypes.addItem("Saving Account");
		accountTypes.addItem("Spending Account");
		addAccount = new JButton("Add Account");
		addAccount.addActionListener(this);
		centerPanel3BL.add(addAccount);
		
		JPanel centerPanel4 = new JPanel();
		centerPanel4.setLayout(new BorderLayout());
		centerPanel4.setBackground(Color.GREEN);
		centerPanel.add(centerPanel4);
		
		JPanel centerPanel4BL = new JPanel();
		centerPanel4BL.setLayout(new GridLayout(2,2));
		centerPanel4BL.setBackground(Color.GREEN);
		centerPanel4.add(centerPanel4BL,BorderLayout.CENTER);
		
		holderName = new JLabel("Holder's Name:");
		centerPanel4BL.add(holderName);
		holderNameField = new JTextField();
		centerPanel4BL.add(holderNameField);
		accountId = new JLabel("Account's Id:");
		centerPanel4BL.add(accountId);
		accountIdField = new JTextField();
		centerPanel4BL.add(accountIdField);
		deleteAccount = new JButton("Delete Account");
		deleteAccount.addActionListener(this);
		centerPanel4.add(deleteAccount,BorderLayout.SOUTH);
		
		JPanel centerPanel5 = new JPanel();
		centerPanel5.setLayout(new BorderLayout());
		centerPanel5.setBackground(Color.GREEN);
		centerPanel.add(centerPanel5);
		
		JPanel centerPanel5BL = new JPanel();
		centerPanel5BL.setLayout(new GridLayout(2,2));
		centerPanel5BL.setBackground(Color.GREEN);
		centerPanel5.add(centerPanel5BL,BorderLayout.CENTER);
		
		withdrawal = new JLabel("Withdrawal Sum:");
		centerPanel5BL.add(withdrawal);
		withdrawalField = new JTextField();
		withdrawalField.setPreferredSize(new Dimension(55,30));
		centerPanel5BL.add(withdrawalField);
		withdrawalButton = new JButton("Withdraw Sum");
		withdrawalButton.addActionListener(this);
		centerPanel5BL.add(withdrawalButton);
		deposit = new JLabel("Deposit Sum:");
		centerPanel5BL.add(deposit);
		depositField = new JTextField();
		depositField.setPreferredSize(new Dimension(55,30));
		centerPanel5BL.add(depositField);
		depositButton = new JButton("Deposit Sum");
		depositButton.addActionListener(this);
		centerPanel5BL.add(depositButton);
		
		modelPerson = new DefaultTableModel();
		modelPerson.addColumn("Name");
		modelPerson.addColumn("Adress");
		modelPerson.addColumn("CNP");
		tablePerson = new JTable(modelPerson);
		scrollPerson = new JScrollPane(tablePerson);
		this.add(scrollPerson, BorderLayout.NORTH);
		
		modelAccount = new DefaultTableModel();
		modelAccount.addColumn("Account Id");
		modelAccount.addColumn("Holder's Name");
		modelAccount.addColumn("Account Type");
		modelAccount.addColumn("Current Sum");
		modelAccount.addColumn("Balance");
		modelAccount.addColumn("Interest Rate");
		tableAccount = new JTable(modelAccount);
		scrollAccount = new JScrollPane(tableAccount);
		centerPanel.add(scrollAccount);
		
		tableAccount.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent evt)
			{
				 row = tableAccount.rowAtPoint(evt.getPoint());
				 col = tableAccount.columnAtPoint(evt.getPoint());
			}
		});
		
		bank = new Bank();
		bank.readPersons();
		bank.readAccounts();
		this.fillTables();
		
		try
		{
			writer = new FileWriter("Notificari.txt",true);
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Eroare la FileWriter");
		}
		
		closeFile = new JButton("Close Stream");
		closeFile.addActionListener(this);
		this.add(closeFile, BorderLayout.SOUTH);
		
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Add Person"))
		{
			addPerson();
		}
		else if(s.equals("Add Account"))
		{
			addAccount();
		}
		else if(s.equals("Delete Person"))
		{
			deletePerson();
		}
		else if(s.equals("Delete Account"))
		{
			deleteAccount();
		}
		else if(s.equals("Withdraw Sum"))
		{
			withdrawMoney();
		}
		else if(s.equals("Deposit Sum"))
		{
			depositMoney();
		}
		else if(s.equals("Close Stream"))
		{
			closeStream();
		}
	}
	
	public void addPerson()
	{
		String id;
		String adress;
		String name;
		
		name = personNameField.getText();
		adress = personAdressField.getText();
		id = personIdField.getText();
		
		Person p = new Person(name,adress,id);
		bank.addPerson(p);
		bank.writePersons();
		fillTables();
	}
	
	public void deletePerson()
	{
		String name = personNameDeleteField.getText();
		boolean found = false;
		for(Person p : bank.getPersons())
		{
			if(p.getName().equals(name))
			{
				bank.getMap().get(p).clear();
				bank.removePerson(p);
				found = true;
				break;
			}
		}
		if(found == false)
		{
			JOptionPane.showMessageDialog(null, "Aceasta persoana nu exista inregistrata la banca!");
		}
		bank.writePersons();
		bank.writeAccounts();
		fillTables();
	}
	
	public void addAccount()
	{
		int accountId = (int)(100 + (Math.random() * (999-100)));
		String name = accountHolderNameField.getText();
		Person p = null;
		for(Person person : bank.getPersons())
		{
			if(person.getName().equals(name))
			{
				p = person;
				break;
			}
		}
		if(p != null)
		{
			String type = String.valueOf(accountTypes.getSelectedItem());
			int money = Integer.parseInt(startingSumField.getText());
			if(type.equals("Saving Account"))
			{
				SavingAccount acc = new SavingAccount(accountId,p,money);
				bank.addAccount(acc);
				bank.writeAccounts();
				fillTables();
			}
			else if(type.equals("Spending Account"))
			{
				SpendingAccount acc = new SpendingAccount(accountId,p,money);
				bank.addAccount(acc);
				bank.writeAccounts();
				fillTables();
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Aceasta persoana nu exista inregistrata la banca!");
		}
	}
	
	public void deleteAccount()
	{
		String name = holderNameField.getText();
		int accountId = Integer.parseInt(accountIdField.getText());
		Person person = null;
		for(Person p : bank.getPersons())
		{
			if(p.getName().equals(name))
			{
				person = p;
				break;
			}
		}
		if(person != null)
		{
			boolean found = false;
			for(Account a : bank.getMap().get(person))
			{
				if(a.getId() == accountId)
				{
					bank.removeAccount(a);
					bank.writeAccounts();
					fillTables();
					found = true;
					break;
				}
			}
			if(found == false)
			{
				JOptionPane.showMessageDialog(null, "Acest account nu exista pe numele de: " + " " + person.getName());
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Acesta persoana nu exista inregistrata la banca!");
		}
	}
	
	public void withdrawMoney()
	{
		if(row == -1 && col == -1)
		{
			JOptionPane.showMessageDialog(null, "Va rugam selectati un cont!");
		}
		else if(row != -1 && col != 0)
		{
			JOptionPane.showMessageDialog(null, "Va rugam selectati id-ul contului pe care doriti sa-l folositi!");
		}
		else
		{
			int accId = (int)tableAccount.getModel().getValueAt(row, col);
			String type = (String)tableAccount.getModel().getValueAt(row,col+2);
			String name =(String)tableAccount.getModel().getValueAt(row,col+1);
			int sum = Integer.parseInt(withdrawalField.getText());
			for(Person p : bank.getPersons())
			{
				if(name.equals(p.getName()))
				{
					for(Account a : bank.getMap().get(p))
					{
						if(a.getId() == accId)
						{
							if(type.equals("Spending Account"))
							{
								((SpendingAccount)a).withdrawMoney(sum);
								bank.writeAccounts();
								fillTables();
								String s = "Ati extras: " + sum + " RON";
								try
								{
									writer.write(s + " " + a.getHolder().getStatus().get(a.getHolder().getStatus().size()-1) + " ");
								}
								catch(IOException ex)
								{
									JOptionPane.showMessageDialog(null, "Eroare la scrierea update-urilor");
								}
								
							}
							else if(type.equals("Saving Account"))
							{
								((SavingAccount)a).withdrawMoney(sum);
								bank.writeAccounts();
								fillTables();
								String s = "Ati extras: " + sum + " RON";
								try
								{
									writer.write(s + " " + a.getHolder().getStatus().get(a.getHolder().getStatus().size()-1) + " ");
								}
								catch(IOException ex)
								{
									JOptionPane.showMessageDialog(null, "Eroare la scrierea update-urilor");
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void depositMoney()
	{
		if(row == -1 && col == -1)
		{
			JOptionPane.showMessageDialog(null, "Va rugam selectati un cont!");
		}
		else if(row != -1 && col != 0)
		{
			JOptionPane.showMessageDialog(null, "Va rugam selectati id-ul contului pe care doriti sa-l folositi!");
		}
		else
		{
			int accId = (int)tableAccount.getModel().getValueAt(row, col);
			String type = (String)tableAccount.getModel().getValueAt(row,col+2);
			String name =(String)tableAccount.getModel().getValueAt(row,col+1);
			int sum = Integer.parseInt(depositField.getText());
			for(Person p : bank.getPersons())
			{
				if(name.equals(p.getName()))
				{
					for(Account a : bank.getMap().get(p))
					{
						if(a.getId() == accId)
						{
							if(type.equals("Spending Account"))
							{
								((SpendingAccount)a).addMoney(sum);
								bank.writeAccounts();
								fillTables();
								String s = "Ati depus: " + sum + " RON";
								try
								{
									writer.write(s + " " + a.getHolder().getStatus().get(a.getHolder().getStatus().size()-1) + " ");
								}
								catch(IOException ex)
								{
									JOptionPane.showMessageDialog(null, "Eroare la scrierea update-urilor");
								}
							}
							else if(type.equals("Saving Account"))
							{
								((SavingAccount)a).addMoney(sum);
								bank.writeAccounts();
								fillTables();
								String s = "Ati depus: " + sum + " RON";
								try
								{
									writer.write(s + " " + a.getHolder().getStatus().get(a.getHolder().getStatus().size()-1) + " ");
								}
								catch(IOException ex)
								{
									JOptionPane.showMessageDialog(null, "Eroare la scrierea update-urilor");
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void fillTables()
	{
		modelPerson.setRowCount(0);
		modelAccount.setRowCount(0);
		for(Person p : bank.getPersons())
		{
			Object [] personRow = {p.getName(), p.getAdress(), p.getId()};
			modelPerson.addRow(personRow);
		}
		for(Person p : bank.getPersons())
		{
			if(bank.getMap().get(p) != null)
			{
				for(Account a : bank.getMap().get(p))
				{
					if(a.getType().equals("Spending Account"))
					{
						Object [] accountRow = {a.getId(), a.getHolder().getName(), a.getType(), a.getSum(), "None","None"};
						modelAccount.addRow(accountRow);
					}
					else if(a.getType().equals("Saving Account"))
					{
						Object [] accountRow = {a.getId(), a.getHolder().getName(), a.getType(), a.getSum(),((SavingAccount)a).getBalance(),((SavingAccount)a).getInterestRate()};
						modelAccount.addRow(accountRow);
					}
				}
			}
		}
	}
	
	public void closeStream()
	{
		try
		{
			writer.close();
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
	}
}
