
import org.junit.Test;

import junit.framework.TestCase;

public class TestBank extends TestCase{
	
	@Test
	public void test()
	{
		Bank bank = new Bank();
		Person p = new Person("George","Zalau","1234567890");
		SpendingAccount account = new SpendingAccount(100,p,500);
		bank.addPerson(p);
		bank.addAccount(account);
		account.addMoney(50);
		String expected = "550";
		Account temp = null;
		for(Account a : bank.getMap().get(p))
		{
			if(a.getId() == account.getId())
			{
				temp = a;
				break;
			}
		}
		assertEquals(expected,String.valueOf(temp.getSum()));
	}
	
	@Test
	public void test2()
	{
		Bank bank = new Bank();
		Person p = new Person("George","Zalau","1234567890");
		SavingAccount account = new SavingAccount(100,p,600);
		bank.addPerson(p);
		bank.addAccount(account);
		account.withdrawMoney(50);
		String expected = "550";
		Account temp = null;
		for(Account a : bank.getMap().get(p))
		{
			if(a.getId() == account.getId())
			{
				temp = a;
				break;
			}
		}
		assertEquals(expected,String.valueOf(temp.getSum()));
	}

}
